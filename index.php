<!doctype html>
<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>My Web</title>
        <link href="assets/css/myweb.css" rel="stylesheet">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <!-- Include roboto.css to use the Roboto web font, material.css to include the theme and ripples.css to style the ripple effect -->
        <link href="assets/css/roboto.min.css" rel="stylesheet">
        <link href="assets/css/material.min.css" rel="stylesheet">
        <link href="assets/css/ripples.min.css" rel="stylesheet">

    </head>

    <body>
      <div id="web-wrapper">
        <header id="web-header">
          <div class="navbar navbar-default">
            <div class="container">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="javascript:void(0)">Brand</a>
              </div>
              <div class="navbar-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="javascript:void(0)">Blog</a></li>
                  <li><a href="javascript:void(0)">Portofolio</a></li>
                  <li><a href="javascript:void(0)">About Me</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="welcoming">
            <h2>Welcome to my site</h2>
          </div>
        </header>
        <section id="web-content" class="container shadow-z-2">
          <h1></h1>
        </section>
      </div>



        <!-- Your site ends -->

        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>

        <script src="assets/js/ripples.min.js"></script>
        <script src="assets/js/material.min.js"></script>
        <script src="assets/js/init.js"></script>

    </body>

</html>
